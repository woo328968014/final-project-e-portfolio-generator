package ssm.controller;

import java.io.File;
import java.io.IOException;

import javafx.stage.FileChooser;
import ssm.LanguagePropertyType;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.EportfolioView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.view.AddTitleView;

import ssm.view.webView;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import ssm.model.Page;
import ssm.model.PageModel;
import ssm.view.ComponentView;
import ssm.model.Component;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & _____________
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private EportfolioView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(EportfolioView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                PageModel slideShow = ui.getPage();
		slideShow.reset();
                //ui.reloadPageInf(slideShow);
               
                
                
                
                
                AddTitleView addTitle=new AddTitleView();
                String title=addTitle.getT();
                
                if(title!=null )
                    ui.getPage().setTitle(addTitle.getT());
                else if(title==null || title=="")
                    ui.getSlideShow().setTitle("New Title");
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                
                ui.slidesEditorPane.getChildren().clear();
                
                ui.updateToolbarControls(saved);
                ui.reloadPagePane(slideShow);
                
                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo provide error message
             eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "ERROR", "ERROR_ADDNEW");
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //@todo provide error message
             eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "ERROR", "ERROR_LOAD");
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    PageModel slideShowToSave = ui.getPage();
	    
            // SAVE IT TO A FILE
            
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "ERROR", "ERROR_SAVE"); 
            
	    return false;
        }
    }
                    
    public boolean handleViewSlideShowRequest() {
       
	    // GET THE SLIDE SHOW TO SHOW
            handleSaveSlideShowRequest();
           
            startWebSite();   
            
            webView view=new webView();
            String path="/Sites/"+ui.getPage().getTitle()+"/"+ui.getPage().getSelected().getcaption()+".html";
            String url=System.getProperty("user.dir")+path;
            url.replace("\\\\", "/");  
            System.out.println("URL: "+url);
            view.start(url);
	    //SlideShowView view=new SlideShowView(ui);

	    return true;
        
    }
    
     public boolean handleExport() {
    
         this.export();
         
         
     return true;
        
    }
    
     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
             eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "ERROR", "ERROR_EXIT");
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork = true; // @todo change this to prompt

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            PageModel slideShow = ui.getPage();
            slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                // @todo
                eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "ERROR", "ERROR_OPEN");
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
    
    public void startWebSite(){
     generateFileFolders();
    // copyData();
     generateHtml();
     generateCSS();
//     generateJS();
    }
    
    public void export(){
        for(int i=0;i<ui.getPage().getPages().size();i++){
            ui.getPage().setSelectedPage(ui.getPage().getPages().get(i));
            generateFileFolders();
            // copyData();
             generateHtml();
             generateCSS();
            //generateJS();
        }
     
    }
    
    
    
     public void generateFileFolders(){
         String fileName=ui.getPage().getTitle();
           //String fileName="AAAA"; 
            File d=new File("Sites");
            if(!d.exists()){
                d.mkdir();
            }else{
               File f=new File("Sites/"+fileName);
               if(!f.exists()){
                   f.mkdir();
                   File js=new File("Sites/"+fileName+"/js");
                   js.mkdir();
                   File css=new File("Sites/"+fileName+"/css");
                   css.mkdir();
                   File img=new File("Sites/"+fileName+"/img");
                   img.mkdir();
               }else
                   System.out.printf("Alread Exist");
            }
               
     }
     public void generateHtml(){
         String fileName=ui.getPage().getTitle();
        
         Page pageToView=ui.getPage().getSelected();
         
         String page1=" not set ";
         
         ObservableList<String> compsHtml;
          compsHtml=FXCollections.observableArrayList();
          String compsFinal="";
          compsHtml.clear();
          
         for(int i=0;i<pageToView.comps.size();i++){
             
             Component tempComp=pageToView.comps.get(i);
             String type=tempComp.type;
             
             if(type.equals("h")){
                 
                 String header=tempComp.headerField;
                 compsHtml.add(  "<h1>"+ header +"</h1>" );
                 compsFinal= compsFinal+compsHtml.get(i);
             }
             if(type.equals("p")){
                 
                 String p=tempComp.paragraphField;
                 String style=tempComp.pfont;
                 compsHtml.add(  "<p"+style+">"+ p +"</p>" );
                 compsFinal= compsFinal+compsHtml.get(i);
             }
             
            if(type.equals("l")){
                String p="";
                p=p+"<ul class=\"list\">";
                for(int j=0;j<tempComp.lists.size();j++){
                    
                  p=p+"<li>"+ tempComp.lists.get(j).getText() +"</li>"  ;
                    
                    
                
                    
                }
                
                p=p+" </ul> ";
                
                
                compsHtml.add( p  );
                compsFinal= compsFinal+compsHtml.get(i);
            }
            
            if(type.equals("Img")){
                
                String copyFrom=tempComp.imgPath;
                String W=tempComp.imgWithField;
                String H=tempComp.imgHightField;
                String copyTo="Sites/"+fileName+"/img/"+tempComp+"_image.jpg";
                copyData(copyFrom,copyTo);
                
                compsHtml.add(  "<img alt=\"Home page\" class=\"homePageImage\" src=\"./img/"+tempComp+"_image.jpg\" style=\"width:"+W+"px;height:"+H+"px;\"     >                                  "  );
                compsFinal= compsFinal+compsHtml.get(i);
                
                
                
            }
            
            if(type.equals("Vid")){
                
                String copyFrom=tempComp.VidPath;
                //System.out.println("VidPath:  "+copyFrom);
                String W=tempComp.vidWithField;
                String H=tempComp.vidHightField;
                String copyTo="Sites/"+fileName+"/img/"+tempComp+"_video.mp4";
                copyData(copyFrom,copyTo);
                
                System.out.println("!!vid start adding: ");
                String p= "  <video class=\"video\" width=\""+W+"\" height=\""+H+"\"  autoplay controls>\n" +
"            <source src=\".img/"+tempComp+"_video.mp4\" type=\"video/mp4\">\n" +

"            Your browser does not support HTML5 video.\n" +
"            \n" +
"           \n" +
"         </video>                ";
                
               
                compsHtml.add(  p );
                
                
                
                compsFinal= compsFinal+compsHtml.get(i);
                
                System.out.println("!!vid added: ");
                
            }
            
           if(type.equals("Slide")){
               
           }      
                 
             
             
         }  
         
            String nav="";
            
            for(int i=0;i<ui.getPage().getPages().size();i++){
                
                String s=" <a class=\"nav\" href=\" "+ui.getPage().getPages().get(i).getcaption()+".html\" >"+ui.getPage().getPages().get(i).getcaption()+"</a>";
                
                nav=nav+s;
                
            }
         
         
         
         
         
         
         
         
         
         
         
         
             System.out.println("!!copyFrom: "+pageToView.banner);
             System.out.println("!!copyTo: "+"Sites/"+fileName+"/img/"+pageToView.getcaption()+"banner.jpg");
             if(!pageToView.banner.isEmpty())
                copyData(pageToView.banner ,"Sites/"+fileName+"/img/"+pageToView.getcaption()+"Banner.jpg");             
             
            
                page1="<!DOCTYPE html>\n" +
                    "<!--\n" +
                    "To change this license header, choose License Headers in Project Properties.\n" +
                    "To change this template file, choose Tools | Templates\n" +
                    "and open the template in the editor.\n" +
                    "-->\n" +
                    "<html>\n" +
                    "    <head>\n" +
                    "        <title>Home</title>\n" +
                    "        <meta charset=\"UTF-8\">\n" +
                    "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                        //+++++++++++++++++++++++++import++++++++++++++++++++++++
                    "        <link href=\"./css/"+pageToView.getcaption()+"style.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    
                         "  <script src=\"./js/Slideshow.js\"></script> "+
                        //+++++++++++++++++++++++++import++++++++++++++++++++++++
                    "    </head>\n" +
                    "    <body>\n" +
                    "       \n" +
                    "        \n" +
                        
                        //++++++++++++++++++++++++banner++++++++++++++++++++++++++
                    "        <div id=\"banner\">\n" +
                    "	    <!-- BANNER NEEDS TO BE SET -->\n" +    //"Sites/"+fileName+"/img/banner.jpg"
                    "           <img alt=\"banner\" class=\"banner\" src=\"./img/"+pageToView.getcaption()+"Banner.jpg\"     style=\"width:500px;height:200px;\"></a>\n" +
                    "        \n" +
                    "        </div>\n" +
                    "        \n" +
                          //++++++++++++++++++++++++banner+++++++++++++++++++++++++++
                        
                    "        <div id=\"navbar\">\n" +
                    "\n" +
                    "	    \n" +
                    "	    <!-- NAVIGATION LINKS GO HERE -->\n" +
                        //+++++++++++++++++++++++++++pages+++++++++++++++++++++
                               nav+
                          //++++++++++++++++++++++++++pages++++++++++++++++++++++++              
                    "	</div>\n" +
                    "        \n" +
                          

                    "        <div id=\"desc\">\n" +
                           //+++++++++++++++++++++comps++++++++++++++++++

                                        "<h1>Title:"+pageToView.title+"</h1>\n" +
                    "			<h1>Student:"+pageToView.name+" </h1>"+ 
                                            compsFinal+
                                        "<h1>"+pageToView.footer+"</h1>\n"+

                          //+++++++++++++++++++=comps+++++++++++++++++++++
                    "        </div>\n" +
                    "        \n" +
                    "        \n" +
                    "    </body>\n" +
                    "</html>"       ; 
                    
            
                     
                     
                     
                     
                     
                     
                     
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
                            String path =  "Sites/"+fileName+"/"+pageToView.getcaption()+".html";


                            FileInputStream fis = null;
                            FileOutputStream fos= null;

                            try{

                                fos= new FileOutputStream(path);
                                int k=0;
                               while(k<page1.length())
                                    fos.write(page1.charAt(k++));

                            }catch(Exception ex){

                            }
                            finally{
                                try{

                                    fos.close();
                                }catch(IOException ex){

                                }
                             }
     
     }
    
    public void generateCSS(){
         String fileName=ui.getPage().getTitle();
         Page pageToView=ui.getPage().getSelected();
         
         String layout="";
         
         if(pageToView.layout.equals("1")){
             layout="#content \n" +
"{\n" +
"	position:absolute;\n" +
"	width:80%;\n" +
"	left:10%;\n" +
"	right:10%;\n" +
"	text-align:left;\n" +
"}\n" +
"\n" +
"#navbar \n" +
"{\n" +
"	position:absolute	;\n" +
"	width:100%;\n" +
"	height:45px;\n" +
"	top:0px;\n" +
"}\n" +
"\n" +
"#banner \n" +
"{\n" +
"	position:absolute;\n" +
"	width:100%;\n" +
"	height:200px;\n" +
"	text-align:center;\n" +
"	padding-top:10px;\n" +
"	padding-bottom:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"	top:50px;\n" +
"}\n" +
"\n" +
"#desc \n" +
"{\n" +
"	position:absolute;\n" +
"	padding-left:1%;\n" +
"	padding-right:0%;\n" +
"	padding-top:0px;\n" +
"	width:100%;\n" +
"	margin-top:0px;\n" +
"	top:218px;\n" +
"}\n" +
"\n" +
"h1{\n" +
"    text-align: center\n" +
"}\n" +
"p{\n" +
"    text-align: center\n" +
"}\n" +
"\n" +
"img.sbu_navbar\n" +
"{\n" +
"	float:right;\n" +
"}\n" +
"\n" +
"img.photo_floating_right\n" +
"{\n" +
"	float:right;\n" +
"	padding-left:20px;\n" +
"}\n" +
"\n" +
"img.photo_floating_left\n" +
"{\n" +
"	float:left;\n" +
"	padding-right:20px;\n" +
"}\n" +
"\n" +
"a.footer\n" +
"{\n" +
"	font-size:18pt;\n" +
"	margin: auto;\n" +
"	padding-top:00px;\n" +
"}\n" +
"\n" +
"img {\n" +
"        width:100%;\n" +
"	border:0;\n" +
"}\n" +
"\n" +
"img.stonybrook {\n" +
"	background-color:white;\n" +
"	padding-left:10px;\n" +
"	float:left;\n" +
"}\n" +
"\n" +
"img.cs {\n" +
"	float:right;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.start_up\n" +
"{\n" +
"	float:right;\n" +
"	margin-left:20px;\n" +
"	margin-right:20px;\n" +
"	margin-bottom:5px;\n" +
"}\n" +
"\n" +
"img.centered {\n" +
"	margin-left:30%;\n" +
"	margin-right:30%;\n" +
"}\n" +
"\n" +
"\n" +
"a.nav, a.open_nav {\n" +
"	display:inline-block;\n" +
"	border:none;\n" +
"	text-decoration:none;\n" +
"	text-align:center;\n" +
"	font-weight:bold;\n" +
"	padding-left:5px;\n" +
"	padding-right:20px;\n" +
"	padding-top:5px;\n" +
"	width:80px;\n" +
"}\n" +
"\n" +
"p.contact_info\n" +
"{\n" +
"	padding-top:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"}\n" +
"\n" +
"td.course_name\n" +
"{\n" +
"	padding-left:10px;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.logo_page\n" +
"{\n" +
"	position:relative;\n" +
"	width:100%;\n" +
"	height:auto;\n" +
"}\n" +
"\n" +
"table \n" +
"{\n" +
"	border-color:#555555;\n" +
"}\n" +
"\n" +
"table.schedule\n" +
"{\n" +
"	margin:auto;\n" +
"	width:100%;\n" +
"}\n" +
"\n" +
"td.sch\n" +
"{\n" +
"	position:relative;\n" +
"	width:20%;\n" +
"	height:150px;\n" +
"	margin-left:0%;\n" +
"	margin-right:0%;\n" +
"	vertical-align:top;\n" +
"}\n" +
"\n" +
"th.sch\n" +
"{\n" +
"	vertical-align:top;\n" +
"	text-align:center;\n" +
"	border-color:#888888;\n" +
"}\n" +
"\n" +
"td.holiday \n" +
"{\n" +
"	background-color:#aaaaaa;\n" +
"	vertical-align:top;\n" +
"}";
         }
         if(pageToView.layout.equals("2")){
             layout="#content \n" +
"{\n" +
"	position:absolute;\n" +
"	width:80%;\n" +
"	left:10%;\n" +
"	right:10%;\n" +
"	text-align:left;\n" +
"}\n" +
"\n" +
"#navbar \n" +
"{   \n" +
"    position: fixed;\n" +
"    top: 0;\n" +
"    left: 0;\n" +
"    width: 120px;\n" +
"    \n" +
"    line-height:30px;\n" +
"    height:100%;\n" +
"    \n" +
"    \n" +
"    padding:1px;\n" +
"    top:0px;\n" +
"}\n" +
"\n" +
"#banner \n" +
"{       \n" +
"	position:absolute;\n" +
"	width:100%;\n" +
"	height:200px;\n" +
"	text-align:center;\n" +
"	padding-top:10px;\n" +
"	padding-bottom:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"	\n" +
"        \n" +
"}\n" +
"\n" +
"#desc \n" +
"{\n" +
"	position:absolute;\n" +
"	padding-left:0%;\n" +
"	padding-right:0%;\n" +
"	padding-top:0px;\n" +
"	\n" +
"	margin-top:0px;\n" +
"	top:218px;\n" +
"        left:120px;\n" +
"}\n" +
"\n" +
"\n" +
"img.photo_floating_right\n" +
"{\n" +
"	float:right;\n" +
"	padding-left:20px;\n" +
"}\n" +
"\n" +
"img.photo_floating_left\n" +
"{\n" +
"	float:left;\n" +
"	padding-right:20px;\n" +
"}\n" +
"\n" +
"a.footer\n" +
"{\n" +
"	font-size:18pt;\n" +
"	text-align:center;\n" +
"	padding-top:00px;\n" +
"}\n" +
"\n" +
"p.paragraph{\n" +
"    font-size:18pt;\n" +
"    text-align:center;\n" +
"}\n" +
"\n" +
"\n" +
"img {\n" +
"    width:100%;\n" +
"	border:0;\n" +
"}\n" +
"\n" +
"img.stonybrook {\n" +
"	background-color:white;\n" +
"	padding-left:10px;\n" +
"	float:left;\n" +
"}\n" +
"\n" +
"img.cs {\n" +
"	float:right;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.start_up\n" +
"{\n" +
"	float:right;\n" +
"	margin-left:20px;\n" +
"	margin-right:20px;\n" +
"	margin-bottom:5px;\n" +
"}\n" +
"\n" +
"img.centered {\n" +
"	margin-left:30%;\n" +
"	margin-right:30%;\n" +
"}\n" +
"\n" +
"\n" +
"a.nav, a.open_nav {\n" +
"	display:inline-block;\n" +
"	border:none;\n" +
"	text-decoration:none;\n" +
"	text-align:center;\n" +
"	font-weight:bold;\n" +
"	padding-left:5px;\n" +
"	padding-right:20px;\n" +
"	padding-top:5px;\n" +
"	width:80px;\n" +
"}\n" +
"";
         }
         if(pageToView.layout.equals("3")){
             layout="#content \n" +
"{\n" +
"	position:absolute;\n" +
"	width:80%;\n" +
"	left:10%;\n" +
"	right:10%;\n" +
"	text-align:left;\n" +
"}\n" +
"\n" +
"#navbar \n" +
"{   \n" +
"    position: fixed;\n" +
"    top: 0%;\n" +
"    right:0;\n" +
"    width: 100px;\n" +
"    \n" +
"    line-height:30px;\n" +
"    height:100%;\n" +
"    \n" +
"    \n" +
"    padding:5px;\n" +
"    \n" +
"}\n" +
"\n" +
"#banner \n" +
"{       \n" +
"	position:absolute;\n" +
"	width:100%;\n" +
"	height:200px;\n" +
"	text-align:center;\n" +
"	padding-top:10px;\n" +
"	padding-bottom:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"	\n" +
"        \n" +
"}\n" +
"\n" +
"#desc \n" +
"{\n" +
"	position:absolute;\n" +
"	padding-left:5%;\n" +
"	padding-right:0%;\n" +
"	padding-top:0px;\n" +
"	width:80%;\n" +
"	margin-top:0px;\n" +
"	top:218px;\n" +
"        left:0px;\n" +
"}\n" +
"\n" +
"video.video{\n" +
"    width: 70%;\n" +
"    \n" +
"}\n" +
"\n" +
"img.head{\n" +
"    width:50%;\n" +
"    \n" +
"}\n" +
"li.list{\n" +
"    left: 40px;\n" +
"}\n" +
"\n" +
"img.photo_floating_right\n" +
"{\n" +
"	float:right;\n" +
"	padding-left:20px;\n" +
"}\n" +
"\n" +
"img.photo_floating_left\n" +
"{\n" +
"	float:left;\n" +
"	padding-right:20px;\n" +
"}\n" +
"\n" +
"p{\n" +
"  text-align: center;      \n" +
"}\n" +
"a.footer\n" +
"{\n" +
"	font-size:18pt;\n" +
"	text-align:center;\n" +
"	padding-top:00px;\n" +
"}\n" +
"\n" +
"p.paragraph{\n" +
"    font-size:18pt;\n" +
"    text-align:center;\n" +
"}\n" +
"\n" +
"\n" +
"img {\n" +
"    width:100%;margin-right:10%;\n" +
"	border:0;\n" +
"}\n" +
"\n" +
"img.stonybrook {\n" +
"	background-color:white;\n" +
"	padding-left:10px;\n" +
"	float:left;\n" +
"}\n" +
"\n" +
"img.cs {\n" +
"	float:right;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.start_up\n" +
"{\n" +
"	float:right;\n" +
"	margin-left:20px;\n" +
"	margin-right:20px;\n" +
"	margin-bottom:5px;\n" +
"}\n" +
"\n" +
"img.centered {\n" +
"	margin-left:30%;\n" +
"	margin-right:30%;\n" +
"}\n" +
"\n" +
"video {\n" +
"	margin-left:15%;\n" +
"	margin-right:20%;\n" +
"}\n" +
"\n" +
"\n" +
"a.nav, a.open_nav {\n" +
"	display:inline-block;\n" +
"	border:none;\n" +
"	text-decoration:none;\n" +
"	text-align:center;\n" +
"	font-weight:bold;\n" +
"	padding-left:5px;\n" +
"	padding-right:20px;\n" +
"	padding-top:5px;\n" +
"	width:80px;\n" +
"}\n" +
"";
         }
         if(pageToView.layout.equals("4")){
             layout="#content \n" +
"{\n" +
"	position:absolute;\n" +
"	width:80%;\n" +
"	left:10%;\n" +
"	right:10%;\n" +
"	text-align:left;\n" +
"}\n" +
"\n" +
"#navbar \n" +
"{\n" +
"	position:absolute;\n" +
"	width:100%;\n" +
"	height:45px;\n" +
"	top:200px;\n" +
"}\n" +
"\n" +
"#banner \n" +
"{\n" +
"	position:absolute;\n" +
"	width:100%;\n" +
"	height:200px;\n" +
"	text-align:center;\n" +
"	padding-top:10px;\n" +
"	padding-bottom:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"	top:0px;\n" +
"}\n" +
"\n" +
"#desc \n" +
"{\n" +
"	position:absolute;\n" +
"	padding-left:1%;\n" +
"	padding-right:0%;\n" +
"	padding-top:0px;\n" +
"	width:100%;\n" +
"        height: 100%;\n" +
"	margin-top:0px;\n" +
"	top:250px;\n" +
"}\n" +
"\n" +
"#slider {\n" +
"	height:400px;\n" +
"	width:650px;\n" +
"	margin: 0px auto;\n" +
"	position:relative;\n" +
"	border-radius:4px;\n" +
"	overflow:hidden;\n" +
"	}\n" +
"#image {\n" +
"        margin-left:15%;\n" +
"	margin-right:30%;\n" +
"	height:70%;\n" +
"	width:70%;\n" +
"	position:absolute;\n" +
"	}\n" +
"#button{ \n" +
"        position: relative;\n" +
"          margin-left: 46%;\n" +
"	\n" +
"}\n" +
"#cap {\n" +
"	text-align: center;\n" +
"	}\n" +
".left {\n" +
"	\n" +
"	position:absolute;\n" +
"	float: top;\n" +
"	\n" +
"	opacity:10;\n" +
"	\n" +
"	}\n" +
".right {\n" +
"	position: absolute;\n" +
"	float: top;\n" +
"	left: 90px;\n" +
"	opacity:10;\n" +
"	}\n" +
".center {\n" +
"	position:absolute;\n" +
"	float: top;\n" +
"	left: 30px;\n" +
"	opacity:10;\n" +
"	}\n" +
"        \n" +
"h1{\n" +
"    text-align: center\n" +
"}\n" +
"p{\n" +
"    text-align: center\n" +
"}\n" +
"\n" +
"img.sbu_navbar\n" +
"{\n" +
"	float:right;\n" +
"}\n" +
"\n" +
"img.photo_floating_right\n" +
"{\n" +
"	float:right;\n" +
"	padding-left:20px;\n" +
"}\n" +
"\n" +
"img.photo_floating_left\n" +
"{\n" +
"	float:left;\n" +
"	padding-right:20px;\n" +
"}\n" +
"\n" +
"a.footer\n" +
"{\n" +
"	font-size:18pt;\n" +
"	margin: auto;\n" +
"	padding-top:00px;\n" +
"}\n" +
"\n" +
"img {\n" +
"        width:100%;\n" +
"	border:0;\n" +
"}\n" +
"\n" +
"img.stonybrook {\n" +
"	background-color:white;\n" +
"	padding-left:10px;\n" +
"	float:left;\n" +
"}\n" +
"\n" +
"img.cs {\n" +
"	float:right;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.start_up\n" +
"{\n" +
"	float:right;\n" +
"	margin-left:20px;\n" +
"	margin-right:20px;\n" +
"	margin-bottom:5px;\n" +
"}\n" +
"\n" +
"img.centered {\n" +
"	margin-left:30%;\n" +
"	margin-right:30%;\n" +
"}\n" +
"\n" +
"\n" +
"a.nav, a.open_nav {\n" +
"	display:inline-block;\n" +
"	border:none;\n" +
"	text-decoration:none;\n" +
"	text-align:center;\n" +
"	font-weight:bold;\n" +
"	padding-left:5px;\n" +
"	padding-right:20px;\n" +
"	padding-top:5px;\n" +
"	width:80px;\n" +
"}\n" +
"\n" +
"p.contact_info\n" +
"{\n" +
"	padding-top:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"}\n" +
"\n" +
"td.course_name\n" +
"{\n" +
"	padding-left:10px;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.logo_page\n" +
"{\n" +
"	position:relative;\n" +
"	width:100%;\n" +
"	height:auto;\n" +
"}\n" +
"";
         }
         if(pageToView.layout.equals("5")){
             layout="#content \n" +
"{\n" +
"	position:absolute;\n" +
"	width:80%;\n" +
"	left:10%;\n" +
"	right:10%;\n" +
"	text-align:left;\n" +
"}\n" +
"\n" +
"#navbar \n" +
"{   \n" +
"    position: fixed;\n" +
"    top: 70%;\n" +
"    left: 0px;\n" +
"    width: 100px;\n" +
"    \n" +
"    line-height:30px;\n" +
"   \n" +
"    \n" +
"    \n" +
"    padding:5px;\n" +
"    \n" +
"}\n" +
"\n" +
"#banner \n" +
"{       \n" +
"	position:absolute;\n" +
"	width:100%;\n" +
"	height:200px;\n" +
"	text-align:center;\n" +
"	padding-top:10px;\n" +
"	padding-bottom:0px;\n" +
"	margin-top:0px;\n" +
"	margin-bottom:0px;\n" +
"	\n" +
"        \n" +
"}\n" +
"\n" +
"#desc \n" +
"{\n" +
"	position:absolute;\n" +
"	padding-left:5%;\n" +
"	padding-right:0%;\n" +
"	padding-top:0px;\n" +
"	\n" +
"	margin-top:0px;\n" +
"	top:218px;\n" +
"        left:120px;\n" +
"}\n" +
"\n" +
"ul{\n" +
"    float: right;\n" +
"}\n" +
"\n" +
"img.head{\n" +
"    width:50%;\n" +
"    \n" +
"}\n" +
"li.list{\n" +
"    left: 40px;\n" +
"}\n" +
"\n" +
"img.photo_floating_right\n" +
"{\n" +
"	float:right;\n" +
"	padding-left:20px;\n" +
"}\n" +
"\n" +
"img.photo_floating_left\n" +
"{\n" +
"	float:left;\n" +
"	padding-right:20px;\n" +
"}\n" +
"\n" +
"a.footer\n" +
"{\n" +
"	font-size:18pt;\n" +
"	text-align:center;\n" +
"	padding-top:00px;\n" +
"}\n" +
"\n" +
"p.paragraph{\n" +
"    font-size:18pt;\n" +
"    text-align:center;\n" +
"}\n" +
"\n" +
"\n" +
"img {\n" +
"    \n" +
"    width:100%;\n" +
"	border:0;\n" +
"}\n" +
"\n" +
"img.stonybrook {\n" +
"	background-color:white;\n" +
"	padding-left:10px;\n" +
"	float:left;\n" +
"}\n" +
"\n" +
"img.cs {\n" +
"	float:right;\n" +
"	padding-right:10px;\n" +
"}\n" +
"\n" +
"img.start_up\n" +
"{\n" +
"	float:right;\n" +
"	margin-left:20px;\n" +
"	margin-right:20px;\n" +
"	margin-bottom:5px;\n" +
"}\n" +
"\n" +
"img.centered {\n" +
"	margin-left:30%;\n" +
"	margin-right:30%;\n" +
"}\n" +
"\n" +
"\n" +
"a.nav, a.open_nav {\n" +
"	display:inline-block;\n" +
"	border:none;\n" +
"	text-decoration:none;\n" +
"	text-align:center;\n" +
"	font-weight:bold;\n" +
"	padding-left:5px;\n" +
"	padding-right:20px;\n" +
"	padding-top:5px;\n" +
"	width:80px;\n" +
"}\n" +
"";
         }
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         String css="body {\n" +
"	background-color:white;\n" +
"	font-family: "+pageToView.font+";}\n" +
                 "p,li,td,th {\n" +
"	font-size:18pt;\n" +
"        \n" +
"}\n" +
"\n" +
"a,p,li,h1,h2,h3,h4,h5,h6 {\n" +
"	color:#003399\n" +
"}\n" +
"\n" +
"\n" +
"\n" +
"#navbar {\n" +
"	background-color:#220000;\n" +
"	margin-top:5px;\n" +
"}\n" +
"\n" +
"#banner {\n" +
"	font-size:24pt;\n" +
"	color:#ffffff;\n" +
"	background-color:#ffffff;\n" +
"}\n" +
"\n" +
"#desc {\n" +
"	background-color:"+pageToView.color+";\n" +
"	color:#ffffff;\n" +
"}\n" +
"\n" +
"a.nav {\n" +
"	color:#ffffff;\n" +
"	font-size:12pt;\n" +
"	font-weight:normal;\n" +
"}\n" +
"\n" +
"a.open_nav {\n" +
"	color:#ffffff;\n" +
"	font-size:18pt;\n" +
"	font-weight:bold;\n" +
"}\n" +
"\n" +
"hr {\n" +
"	color:#111155;\n" +
"}\n" +
"\n" +
"a.open_nav:hover {\n" +
"\n" +
"}\n" +
"\n" +
"a.nav:hover{\n" +
"	color:rgb(118,143,165);\n" +
"}\n" +
"\n" +
"a:hover {\n" +
"	color:rgb(118,143,165);\n" +
"}\n" +
"\n" +
"a {\n" +
"	text-decoration:none;\n" +
"	color:#ffffff;\n" +
"        font-size:18pt;\n" +
"	font-weight:bold;\n" +
"}"+
                 
"}"
                 + layout
                 + "";
                            //"Sites/"+fileName+"/"+pageToView.getcaption()+".html";
                            String path =  "Sites/"+fileName+"/css/"+pageToView.getcaption()+"style.css";
                            System.out.println("css path: "+path);

                            FileInputStream fis = null;
                            FileOutputStream fos= null;

                            try{

                                fos= new FileOutputStream(path);
                                int i=0;
                               while(i<css.length())
                                    fos.write(css.charAt(i++));

                            }catch(Exception ex){

                            }
                            finally{
                                try{

                                    fos.close();
                                }catch(IOException ex){

                                }
                             }
     } 
    
    public void generateJS(){
         String fileName=ui.getSlideShow().getTitle();
         int size=ui.getSlideShow().getSlides().size();
         String caption="";
         
        for(int i=0;i<size;i++){
            caption=caption+"\""+ui.getSlideShow().getSlides().get(i).getcaption() +"\"";
            if(i!=size-1)
                caption=caption+",";
        }
         
         String js="var imageCount = 0;\n" +
               "var total ="   +size+  ";\n" +
               "var click = 0;\n" +
               "var inter;\n" +
               "var caption=["+caption+"];\n" +
               "\n" +
               "function photo(x) {\n" +
               "	var image = document.getElementById('image');\n" +
               "	imageCount = imageCount + x;\n" +
               "	\n" +
               "	if(imageCount >= total){imageCount = 0;}\n" +
               "	if(imageCount < 0){imageCount = total-1;}	\n" +
               "	\n" +
               "	image.src = \"img/\"+ imageCount  +\".jpg\";\n" +
               "	document.getElementById(\"cap\").innerHTML =caption[imageCount]; \n" +
               "	}\n" +
               "	\n" +
               "\n" +
               "\n" +
               "function play() {\n" +
               "	\n" +
               "	click=click+1;\n" +
               "	if(click%2==1){\n" +
               "	\n" +
               "		document.getElementById(\"play_button\").innerHTML =\"Pause\";\n" +
               "		\n" +
               "		inter=setInterval(\"photo(1)\",3000);\n" +
               "		\n" +
               "		\n" +
               "	}\n" +
               "	else{\n" +
               "		clearInterval(inter);\n" +
               "		document.getElementById(\"play_button\").innerHTML =\"Play\";\n" +
               "		click=0;\n" +
               "	}\n" +
               "		\n" +
               "}	";
                            String path =  "Sites/"+fileName+"/js/Slideshow.js";


                            FileInputStream fis = null;
                            FileOutputStream fos= null;

                            try{

                                fos= new FileOutputStream(path);
                                int i=0;
                               while(i<js.length())
                                    fos.write(js.charAt(i++));

                            }catch(Exception ex){

                            }
                            finally{
                                try{

                                    fos.close();
                                }catch(IOException ex){

                                }
                             }
     } 
    
    public void copyData(String copyFrom, String copyTo){
        
        
           

                 System.out.println("Copy succeed");
                 System.out.println("Copy from:  "+copyFrom);
                 System.out.println("Copy to:  "+copyTo);
                    
                    
                    
                FileInputStream fis = null;
                FileOutputStream fos= null;

                try{
                    fis= new FileInputStream(copyFrom);
                    fos= new FileOutputStream(copyTo);
                    int i;
                    while((i=fis.read())!=-1){
                        fos.write(i);
                        System.out.println("Copying: "+i);
                        
                    }
                }catch(Exception ex){

                }
                finally{
                    try{
                        fis.close();
                        fos.close();
                    }catch(IOException ex){

                    }



                  
                
        }
    }
    
}


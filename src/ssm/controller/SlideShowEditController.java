package ssm.controller;

import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.DEFAULT_IMAGE_CAPTION;

import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.SlideShowModel;
import ssm.view.EportfolioView;
import ssm.model.PageModel;
/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowEditController {
    // APP UI
    private EportfolioView ui;
    
   
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(EportfolioView initUI) {
	ui = initUI;
       
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
     
   
    public void processAddSlideRequest(SlideShowModel slideShow) {
       
	//SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES ,props.getProperty(DEFAULT_IMAGE_CAPTION.toString()));
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
    public void processAddPageRequest(){
        PageModel page=ui.getPage();
        page.addPage();
        ui.reloadPageInf(page);
        ui.component.updataUI(ui.getPage().getSelected());
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
    public void processAddSlideRequest2() {
       
	SlideShowModel slideShow = ui.getSlideShow();
	
	
        ui.reloadSlideShowPane(slideShow);
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
    public void processRemovePageRequest() {
	PageModel page=ui.getPage();
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.removePage();
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
    public void processRemoveSlideRequest(SlideShowModel slideShow) {
	
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.removeSlide();
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
    public void  processMoveUpSlideRequest() {
	PageModel page=ui.getPage();
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.moveUpSlide();
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
   public void  processMoveDownSlideRequest() {
	PageModel page=ui.getPage();
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.moveDownSlide();
        ui.updateWorkspace(ui);
        ui.updateToolbarControls(false);
    }
    
    
    
    
    
}

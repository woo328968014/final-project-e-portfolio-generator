package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.EportfolioView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 */
public class page {
    EportfolioView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    
    public page(EportfolioView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
     public EportfolioView getUi() {
	return ui;
    }
     
    public Slide getSelectedSlide() {
	return selectedSlide;
    }
    
    public int getIndex(){
        return slides.indexOf(selectedSlide);
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide 
     * to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath, String caption) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath,caption);
       
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
        ui.reloadSlideShowPane(this);
    }
    
    public void addSlide(   Slide slideToAdd) {
	
       
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
        ui.reloadSlideShowPane(this);
    }
    
    
    
    
    
    public void addCaption(String caption){
        
    }
    
     public void removeSlide(   ) {
	
	slides.remove(selectedSlide);
        if(slides.isEmpty())
            selectedSlide = null;
        else
            selectedSlide=slides.get(0);
	ui.reloadSlideShowPane(this);
    }
     
      public void moveUpSlide(   ) {
	if(slides.isEmpty())
            selectedSlide = null;
        else{
            int index = slides.indexOf(selectedSlide);
            if(index>0){
                Slide temp=slides.get(index-1); 
                slides.set(index-1, selectedSlide);
                slides.set(index, temp);
            }
               
        }
        ui.reloadSlideShowPane(this);
    }
      
       public void moveDownSlide(   ) {
	if(slides.isEmpty())
            selectedSlide = null;
        else{
            int index = slides.indexOf(selectedSlide);
            
            if(index+1<=slides.size()){
                 Slide temp=slides.get(index+1);
                 slides.set(index+1, selectedSlide);
                slides.set(index, temp);
            }
              
        }
        ui.reloadSlideShowPane(this);
    }
       
       public void previousSlide(   ) {
	if(slides.isEmpty())
            selectedSlide = null;
        else{
            int index = slides.indexOf(selectedSlide);
            
            if(index+1<=slides.size())
                selectedSlide=slides.get(index+1);
        }
        ui.reloadSlideShowPane(this);
       }
       
       public void showPreviousSlide( int index  ) {
	
        ui.reloadSlideShowPane(this);
       }

  
       
       
       
       
       
}
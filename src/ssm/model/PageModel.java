package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.EportfolioView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PageModel {
    EportfolioView ui;
    String title;
    
    ObservableList<Page> pages;
    Page selected;
    
    Boolean captionEntered;
    Page initPage;
    
    public PageModel(EportfolioView initUI) {
	ui = initUI;
	pages = FXCollections.observableArrayList();
        //initPage=new Page("New");
       // pages.set(0, initPage);
        //selected=pages.set(0, initPage);
	reset();	
    }

    // ACCESSOR METHODS
 
   
    public boolean isPageSelected() {
	return selected != null;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
     public EportfolioView getUi() {
	return ui;
    }
     
    public Page getSelected() {
	return selected;
    }
    
    public int getIndex(){
        return pages.indexOf(selected);
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedPage(Page initSelectedSlide) {
	selected = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	pages.clear();
        
        
        
        
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selected = null;
    }

    /**
     * Adds a slide 
     * to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addPage() {
	Page pageToAdd = new Page("New Page");
       
	pages.add(pageToAdd);
	selected = pageToAdd;
       ui.reloadPagePane(this);
    }
    
   
    
    
    
    
    
    public void addCaption(String caption){
        
    }
    
     public void removePage(   ) {
	
	pages.remove(selected);
        if(pages.isEmpty())
            selected = null;
        else
            selected=pages.get(0);
	ui.reloadPagePane(this);
    }
     
      public void moveUpSlide(   ) {
	if(pages.isEmpty())
            selected = null;
        else{
            int index = pages.indexOf(selected);
            if(index>0){
                Page temp=pages.get(index-1); 
                pages.set(index-1, selected);
                pages.set(index, temp);
            }
               
        }
       ui.reloadPagePane(this);
    }
      
       public void moveDownSlide(   ) {
	if(pages.isEmpty())
            selected = null;
        else{
            int index = pages.indexOf(selected);
            
            if(index+1<=pages.size()){
                 Page temp=pages.get(index+1);
                 pages.set(index+1, selected);
                pages.set(index, temp);
            }
              
        }
        ui.reloadPagePane(this);
    }
       
       public void previousSlide(   ) {
	if(pages.isEmpty())
            selected = null;
        else{
            int index = pages.indexOf(selected);
            
            if(index+1<=pages.size())
                selected=pages.get(index+1);
        }
        ui.reloadPagePane(this);
       }
       
       public void showPreviousSlide( int index  ) {
	
        ui.reloadPagePane(this);
       }

  
       
       
       
       
       
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.SlideShowModel;
import javafx.scene.layout.VBox;
import ssm.model.Component;
import ssm.model.Page;
import ssm.model.PageModel;

/**
 *
 * @author woo32
 */
public class ComponentView extends VBox{
    
    SlideShowEditController editController;
    Button saveButton;
    int selected;
    int index=0;
    int slidesIndex;
    public ObservableList<ScrollPane> pane;
    public ObservableList<Component>comp;
    int removeCounter;
    EportfolioView ui;
    VBox vbox;
    PageModel model;
    public ComponentView(PageModel pageModel,EportfolioView initView){
        getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        slidesIndex=1;
        model=pageModel;
        pane = FXCollections.observableArrayList();
        comp = FXCollections.observableArrayList();
        ui=initView;
    }
    public void removeSelected(){
       
        if(selected>=0 && selected<=comp.size()){
                comp.remove(selected);
                pane.remove(selected);
                
                
                
        }
        if(comp.size()>0)
            comp.get(0).selected=true;
        this.updataUI();
        
    }
    
    public void getData(){
        model.getSelected().comps.clear();
        model.getSelected().comps.addAll(comp);
        model.getSelected().panes.clear();
        model.getSelected().panes.addAll(pane);
        System.out.println("page selected comp size "+model.getSelected().comps.size());
        
        if(model.getSelected().comps.size()>0)
            System.out.println(model.getSelected().comps.get(0).headerField);
        
    }
    
    public void addHeader(){
        
         Label Header=new Label("Header:");
         TextField headerField=new TextField();
         
         saveButton=new Button("Save");
         vbox=new VBox();
         vbox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
         ScrollPane headerPane= new ScrollPane(vbox);
         
         vbox.getChildren().add(Header);
         vbox.getChildren().add(headerField);
         vbox.getChildren().add(saveButton);
         pane.add(headerPane);
         Component newComp=new Component();
         
         newComp.headerField=headerField.getText();
         
         
         comp.add(pane.indexOf(headerPane),newComp);
         comp.get(pane.indexOf(headerPane)).setType("h");
         
        
        
        saveButton.setOnMouseClicked(e->{
        
            System.out.println("header comps saved");
            comp.get(pane.indexOf(headerPane)).headerField=headerField.getText();
            comp.get(pane.indexOf(headerPane)).compPane=headerPane;
            this.updataUI();
         });
         
         
         
          vbox.setOnMouseClicked(e->{
               
              // vbox.setStyle("-fx-background-color: blue");
              newComp.selected=true;
               
              System.out.println(selected);
              this.updataUI();
              
              
              
        });
         
         
        }
    
    public void addParagraph(){
        
        Label paragraph=new Label("Enter paragraph:");
        TextArea paragraphField=new TextArea();
        Label font = new Label("Chose Font:");
        Button font1=new Button("font1");
        Button font2=new Button("font2");
        Button font3=new Button("font3");
        Button font4=new Button("font4");
        Button font5=new Button("font5");
        Button saveButton=new Button("Save");
        
        Label URL=new Label("\nURL:");
        TextField linkField=new TextField();
        Button addLink=new Button("add Hyberlink URL");
         
        VBox vbox=new VBox();
        vbox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        ScrollPane pPane= new ScrollPane(vbox); 
        
        HBox fontHBox=new HBox();
        fontHBox.getChildren().add(font1);
        fontHBox.getChildren().add(font2);
        fontHBox.getChildren().add(font3);
        fontHBox.getChildren().add(font4);
        fontHBox.getChildren().add(font5);
        
        vbox.getChildren().add(paragraph);
        vbox.getChildren().add(paragraphField);
        vbox.getChildren().add(font);
        vbox.getChildren().add(fontHBox);
        vbox.getChildren().add(URL);
        vbox.getChildren().add(linkField);
        vbox.getChildren().add(addLink);
        vbox.getChildren().add(saveButton);
        pane.add(pPane);
           Component newComp=new Component();
         comp.add(pane.indexOf(pPane),newComp);;
         comp.get(pane.indexOf(pPane)).setType("p");
          vbox.setOnMouseClicked(e->{
               
              // vbox.setStyle("-fx-background-color: blue");
              newComp.selected=true;
               
              System.out.println(selected);
              this.updataUI();
              
            
              
        });
          
          font1.setOnMouseClicked(e->{
              comp.get(pane.indexOf(pPane)).pfont="style=\"font-family:verdana;\"";
          });
          
          font2.setOnMouseClicked(e->{
              comp.get(pane.indexOf(pPane)).pfont="style=\"font-family:courier;\" ";
          });
          
          font3.setOnMouseClicked(e->{
              comp.get(pane.indexOf(pPane)).pfont="style=\"font-family:Geneva;\"";
          });
          
          font4.setOnMouseClicked(e->{
              comp.get(pane.indexOf(pPane)).pfont="style=\"font-family:Times New Roman;\"";
          });
          
          font5.setOnMouseClicked(e->{
              comp.get(pane.indexOf(pPane)).pfont="style=\"font-family:Palatino Linotype;\"";
          });
          addLink.setOnMouseClicked(e->{
             
          });
          
          saveButton.setOnMouseClicked(e->{
        
            System.out.println("header p saved");
            comp.get(pane.indexOf(pPane)).paragraphField=paragraphField.getText();
            comp.get(pane.indexOf(pPane)).linkField=linkField.getText();
            
            
            comp.get(pane.indexOf(pPane)).compPane=pPane;
            
            this.updataUI();
         });
          
          
          
          
          
          
    }
    
     public void addList(){
         
         
         ObservableList<Label>lists;
         lists = FXCollections.observableArrayList();
       
         Label list=new Label("\nEnter List:");
         Button addList=new Button("add list");
         Button romoveList=new Button("romve list");
         TextField listField=new TextField();
         Button saveButton=new Button("Save");
         
         
        VBox vbox=new VBox();
        
        vbox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        ScrollPane listPane= new ScrollPane(vbox); 
        vbox.getChildren().add(list);
        vbox.getChildren().add(listField);
        
      
        vbox.getChildren().add(addList);
        vbox.getChildren().add(romoveList);
        vbox.getChildren().add(saveButton);
        
        pane.add(listPane);
        int n=pane.indexOf(listPane);
        Component newComp=new Component();
        comp.add(pane.indexOf(listPane),newComp);
        comp.get(pane.indexOf(listPane)).setType("l");
        pane.get(pane.indexOf(listPane));
                
         addList.setOnMouseClicked(e->{
             Label listName=new Label(listField.getText());
             lists.add(listName);
             vbox.getChildren().clear();
             vbox.getChildren().add(list);
             vbox.getChildren().add(listField);
         
         
            for(int i=0;i<lists.size();i++){
                vbox.getChildren().add(lists.get(i));
            System.out.println("added List "+lists.get(i).getText());
            }
      
            vbox.getChildren().add(addList);
            vbox.getChildren().add(romoveList);
            vbox.getChildren().add(saveButton);
            
        pane.remove(n);
        pane.add(n,listPane);
       
            
            
            
             
             
             
             
             this.updataUI();
             
             
         });
         saveButton.setOnMouseClicked(e->{
             System.out.println("list comps saved");
             
             
                
             
             
             
            comp.get(pane.indexOf(listPane)).lists=lists;
            comp.get(pane.indexOf(listPane)).compPane=listPane;
            
            this.updataUI();
         });
         
          romoveList.setOnMouseClicked(e->{
             
             vbox.getChildren().clear();
             vbox.getChildren().add(list);
             vbox.getChildren().add(listField);
             
             if(lists.size()>0)
                lists.remove(lists.size()-1);
            
            for(int i=0;i<lists.size();i++){
                vbox.getChildren().add(lists.get(i));
                System.out.println("added List"+lists.get(i));
            }
      
            vbox.getChildren().add(addList);
            vbox.getChildren().add(romoveList);
            pane.remove(n);
            pane.add(n,listPane);
              
              this.updataUI();
          });
         
         
         
          vbox.setOnMouseClicked(e->{
               
              // vbox.setStyle("-fx-background-color: blue");
              newComp.selected=true;
               
              System.out.println(selected);
              this.updataUI();
              
              
              
        });
         
     }
     
     
    public void addImage(){
        Button saveButton=new Button("Save");
        Label image=new Label("\nLoad Image:");
        Button addImage=new Button("Chose file");
        Label imgCaption=new Label("Caption:");
        
        
        Label imgWith=new Label("With:");
        Label imgHight=new Label("Height:");
        Button imgFloatL=new Button("Float left");
        Button imgFloatR=new Button("Float right");
        Button imgFloatC=new Button("Float center");
        TextField imgWithField=new TextField();
        TextField imgHightField=new TextField();
        HBox imgLayoutHBox=new HBox();
        imgLayoutHBox.getChildren().add(imgFloatL);
        imgLayoutHBox.getChildren().add(imgFloatC);
        imgLayoutHBox.getChildren().add(imgFloatR);
        
        HBox imgSizeHBox=new HBox();
        imgSizeHBox.getChildren().add(imgHight);
        imgSizeHBox.getChildren().add(imgHightField);
        imgSizeHBox.getChildren().add(imgWith);
        imgSizeHBox.getChildren().add(imgWithField);
        TextField imgCaptionField=new TextField();
        
        VBox vbox=new VBox();
        vbox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        ScrollPane imgPane= new ScrollPane(vbox); 
        
        vbox.getChildren().add(image);
        vbox.getChildren().add(addImage);
        vbox.getChildren().add(imgCaption);
        vbox.getChildren().add(imgCaptionField);
        vbox.getChildren().add(imgSizeHBox);
        vbox.getChildren().add(imgLayoutHBox);
        vbox.getChildren().add(saveButton);
        
        ImageSelectionController imageController = new ImageSelectionController();
        
        
        
        
        pane.add(imgPane);
        
         Component newComp=new Component();
         comp.add(pane.indexOf(imgPane),newComp);
         comp.get(pane.indexOf(imgPane)).setType("Img");
         
         
          addImage.setOnMouseClicked(e->{
              String path = imageController.processSelectImage();
              
              comp.get(pane.indexOf(imgPane)).imgPath=path;
          });
         
         
         
         
          vbox.setOnMouseClicked(e->{
               
              // vbox.setStyle("-fx-background-color: blue");
              newComp.selected=true;
               
              System.out.println(selected);
              this.updataUI();
              
         
              
        });
          
          
          
          
          
        saveButton.setOnMouseClicked(e->{
             System.out.println("Img comps saved");
             
             
                
             
             
             
            comp.get(pane.indexOf(imgPane)).imgCaptionField=imgCaptionField.getText();
            comp.get(pane.indexOf(imgPane)).imgHightField=imgHightField.getText();
            comp.get(pane.indexOf(imgPane)).imgWithField=imgWithField.getText();
            comp.get(pane.indexOf(imgPane)).compPane=imgPane;
            this.updataUI();
         });     
        
     } 
    
    public void addVideo(){
        
        Button saveButton=new Button("Save");
        Label video=new Label("\nLoad Video:");
        Button addVideo=new Button("Chose file");
        Label vidCaption=new Label("Caption:");
        Label vidWith=new Label("With:");
        Label vidHight=new Label("Height:");
        TextField vidWithField=new TextField();
        TextField vidHightField=new TextField();
        TextField vidCaptionField=new TextField();
        HBox vidSizeHBox=new HBox();
        
        vidSizeHBox.getChildren().add(vidHight);
        vidSizeHBox.getChildren().add(vidHightField);
        vidSizeHBox.getChildren().add(vidWith);
        vidSizeHBox.getChildren().add(vidWithField);
        
        VBox vbox=new VBox();
        vbox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        ScrollPane vidPane= new ScrollPane(vbox); 
        
        vbox.getChildren().add(video);
        vbox.getChildren().add(addVideo);
        vbox.getChildren().add(vidCaption);
        vbox.getChildren().add(vidCaptionField);
        vbox.getChildren().add(vidSizeHBox);
        vbox.getChildren().add(saveButton);
        pane.add(vidPane);
        ImageSelectionController imageController = new ImageSelectionController();
          Component newComp=new Component();
         comp.add(pane.indexOf(vidPane),newComp);
         comp.get(pane.indexOf(vidPane)).setType("Vid");
          vbox.setOnMouseClicked(e->{
               
              // vbox.setStyle("-fx-background-color: blue");
              newComp.selected=true;
               
              System.out.println(selected);
              this.updataUI();
           
              
              
        });
          
          addVideo.setOnMouseClicked(e->{
              String path = imageController.processSelectImage();
              
              comp.get(pane.indexOf(vidPane)).VidPath=path;
          });   
         saveButton.setOnMouseClicked(e->{
             System.out.println("Img comps saved");
             
             
                
             
             
             
            comp.get(pane.indexOf(vidPane)).imgCaptionField=vidCaptionField.getText();
            comp.get(pane.indexOf(vidPane)).imgHightField=vidHightField.getText();
            comp.get(pane.indexOf(vidPane)).imgWithField=vidWithField.getText();
            comp.get(pane.indexOf(vidPane)).compPane=vidPane;
            this.updataUI();
         });      
          
    }
    
    public void addSlides(){
        
        slidesIndex++;
        int i=slidesIndex;
        SlideShowModel slideShow=new SlideShowModel(ui);
        
         Label slideLabel=new Label("Slides:"+i);
         Button addSlide=new Button("Add");
         Button removeSlide=new Button("Remove");
         Button save=new Button("Save");
        HBox hbox=new HBox();
        hbox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        ScrollPane slidePane= new ScrollPane(hbox); 
        
        
        
        
        hbox.getChildren().add(slideLabel);
        hbox.getChildren().add(addSlide);
        hbox.getChildren().add(removeSlide);
        hbox.getChildren().add(save);
        
        
        
       slidePane.setMinWidth(200);
        pane.add(slidePane);
        
          Component newComp=new Component();
          newComp.slideModels.add(slideShow);
         comp.add(pane.indexOf(slidePane),newComp);
         comp.get(pane.indexOf(slidePane)).setType("Slide");
         slidePane.setOnMouseClicked(e->{
               
              // vbox.setStyle("-fx-background-color: blue");
              newComp.selected=true;
              ui.reloadSlideShowPane(slideShow); 
              System.out.println(selected);
              this.updataUI();
              
              
              
        });
          editController = new SlideShowEditController(ui);
          addSlide.setOnMouseClicked(e->{
             
             editController.processAddSlideRequest(slideShow); 
          });
           removeSlide.setOnMouseClicked(e->{
             
             editController.processRemoveSlideRequest(slideShow); 
          });
           
          save.setOnMouseClicked(e->{
             System.out.println("Slide comps saved");
             
             
                
             
             
             
            
            comp.get(pane.indexOf(slidePane)).compPane=slidePane;
            this.updataUI();
         });       
           
          
          
          
    }
     
    public VBox updataUI(){
        this.getData();
        this.getChildren().clear();
        System.out.print("pane size" +pane.size());
        System.out.print("comp size" +comp.size());
        
        for(int i=0; i<pane.size();i++){
            pane.get(i).setStyle("-fx-background-color: ");
            
            
            
            if(comp.get(i).selected==true){
                
                selected=i;
                pane.get(i).setStyle("-fx-background-color: rgb(50, 150, 930)");
                comp.get(i).selected=false;
               //System.out.print("reset");
            }
            
            this.getChildren().add(pane.get(i));
            
        }
            //System.out.print(pane.size());
        System.out.println(comp.size()+"  "+pane.size());
        
        return this;
    }

    public void updataUI(Page page){
            
           
            this.comp.clear();
            this.pane.clear();
            
            if(model.getSelected()!=null){
                comp.addAll(model.getSelected().comps);
                pane.addAll(model.getSelected().panes);
            }
            
            
            
           
            this.updataUI();

            
            
        }
    
    public void reset(){
        this.comp.clear();
            
            this.pane.clear();
            
           
            this.updataUI();
    }
    
    public void reLoad(){
        
        System.out.println("comp size "+comp.size());
        for(int i=0; i<comp.size();i++){
            
            
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
}

package ssm.view;


import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.Label;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class ErrorView extends Stage {
    
    VBox vbox; 
    Button okButton;
    
    Label label2;
    LanguagePropertyType errorMessage;
   
    public ErrorView(String title, String message) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	//this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        
        
       
        this.setTitle(props.getProperty(errorMessage.ERROR.toString()));
            label2=new Label();
        if(this.getTitle()==null || this.getTitle()=="")
            this.setTitle("WARNING");
            
        if(message.equals("ERROR_DATA_FILE_LOADING"))
            label2=new Label("Language load failed");
        else if(message.equals("ERROR_PROPERTIES_FILE_LOADING"))
            label2=new Label(props.getProperty(errorMessage.ERROR_PROPERTIES_FILE_LOADING.toString()));
        else if(message.equals("ERROR_NO_FILE_SELECTED"))
            label2=new Label(props.getProperty(errorMessage.ERROR_NO_FILE_SELECTED.toString()));
        else if(message.equals("ERROR_ADDNEW"))
            label2=new Label(props.getProperty(errorMessage.ERROR_ADDNEW.toString()));        
        else if(message.equals("ERROR_LOAD"))
            label2=new Label(props.getProperty(errorMessage.ERROR_LOAD.toString()));        
        else if(message.equals("ERROR_SAVE"))
            label2=new Label(props.getProperty(errorMessage.ERROR_SAVE.toString()));        
        else if(message.equals("ERROR_EXIT"))
            label2=new Label(props.getProperty(errorMessage.ERROR_EXIT.toString()));       
        else if(message.equals("ERROR_OPEN"))
            label2=new Label(props.getProperty(errorMessage.ERROR_OPEN.toString()));   
        else if(message.equals("ERROR_START"))
            label2=new Label("Programe start failed"); 
        else
            label2=new Label(props.getProperty(errorMessage.ERROR_UNKNOW.toString()));
        
         this.setAlwaysOnTop(true);       
                
	okButton=new Button("OK");
        
        
        vbox=new VBox();
	
        
        Scene scene = new Scene(vbox);
        this.setHeight(100);
	this.setWidth(400);
        vbox.getChildren().addAll(label2,okButton);
        this.setScene(scene);
        this.show();
        
	okButton.setOnAction(e ->{
            
            this.hide();
        });
        

	
    }
    
    
   
    
}
package ssm.view;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
 
 
public class webView extends Stage{
    private Scene scene;
    public void start(String url) {
        // create the scene
        
        
        this.setTitle("Web View");
        
        scene = new Scene(new Browser(url),900,690, Color.web("#666970"));
        
        this.setScene(scene);
        this.setResizable(false);
        
        this.show();
    }
 
    
}
class Browser extends Region {
    
    final WebView browser = new WebView();
    final WebEngine webEngine = browser.getEngine();
     
    public Browser(String url) {
        //apply the styles
        
        getStyleClass().add("browser");
        // load the web page
        webEngine.load( "file:///"+url );
        
        //add the web view to the scene
        getChildren().add(browser);
 
    }
    private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }
 
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }
 
    @Override protected double computePrefWidth(double height) {
        return 800;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 630;
    }
}
package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.PATH_ICONS;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class AddTitleView extends Stage {
    String language;
    String title;
    VBox vbox; 
    Button okButton;
    ComboBox box; 
    Label label;
    Label label2;
    TextField titleTextField;
    LanguagePropertyType message;
    public AddTitleView() {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	//this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        label=new Label("Please chose your language:");
        title="NEW TITLE";
        label2=new Label(props.getProperty(message.SET_TITLE.toString()));
	okButton=new Button("OK");
        titleTextField=new TextField(title);
        
        vbox=new VBox();
	
        
        Scene scene = new Scene(vbox);
       
        vbox.getChildren().addAll(label2,titleTextField,okButton);
        this.setScene(scene);
        titleTextField.setOnMouseClicked(e->{
            if(title.equals("NEW TITLE"))
                titleTextField.clear();
        });
        
        titleTextField.setOnKeyPressed(e->{
            switch (e.getCode()) {
                 case ENTER:
                     title=titleTextField.getText();
                    this.hide();
            }
        });
        
	okButton.setOnAction(e ->{
            
            title=titleTextField.getText();
            this.hide();
        });
        
        this.setAlwaysOnTop(true);
        String imagePath = "file:" + PATH_ICONS + ICON_VIEW_SLIDE_SHOW;
        this.getIcons().add(new Image(imagePath));
        this.showAndWait();
        
	
    }
    
     public String getT(){
       //this.showAndWait();
        return title;
    }
    
    public String getLanguage(){
       this.showAndWait();
        return language;
    }
    
   
    
}
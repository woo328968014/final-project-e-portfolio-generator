package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Page;
import ssm.model.PageModel;
import ssm.model.SlideShowModel;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PageEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Page page;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    VBox workSpaceVBox;
    Label captionLabel;
    Label caption;
    Label studentName;
    Label banner;
    ImageView bannerView;
    TextField captionTextField;
    TextField nameField;
    String userCaption;
    
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public PageEditView(Page initPage, PageModel model) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
       // System.out.print(initSlide.getCaptionEntered());
        
        
	
                
	// KEEP THE SLIDE FOR LATER
	page = initPage;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	

	// SETUP THE CAPTION CONTROLS
       
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
	captionVBox.getChildren().add(captionLabel);
        if(!page.getcaption().isEmpty())
            initPage.setCaptionEntered(true);
        
        if(page.getCaptionEntered()==false){
            captionTextField = new TextField();
            captionVBox.getChildren().add(captionTextField);
            
        }else{
            captionTextField = new TextField();
            captionTextField.setText(page.getcaption());
            captionVBox.getChildren().add(captionTextField);
        }
        captionVBox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        workSpaceVBox = new VBox();
        
        studentName=new Label("Student Name:");
        banner=new Label("Click Page icon to set Banner");
        bannerView=new ImageView();
        nameField=new TextField();
        
        
        getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
        
        
        

	// SETUP THE EVENT HANDLERS
       if(!captionTextField.isFocused()){
           initPage.setCaptionEntered(true);
           this.updateCaption();
           
       }
       
       captionTextField.setOnMouseClicked(e->{
           if(captionTextField.getText().equals("ENTER CAPTION"))
               captionTextField.clear();
       });
       
       captionTextField.setOnAction(e->{
          
           initPage.setCaptionEntered(true);
           this.updateCaption();
           
           captionTextField.setDisable(true);
       }
       );
        
        
        
       captionVBox.setOnMouseClicked(e->{
               
               model.setSelectedPage(initPage);
               
              // this.setStyle("-fx-background-color: blue");
              model.getUi().reloadPagePane(model);
              //System.out.println("header "+model.getSelected().comps.get(0).headerField+"");
              //System.out.println("layout "+model.getSelected().layout+"");
              //System.out.println("bannerPath "+model.getSelected().banner+"");
             model.getUi().reloadPageInf(model);
             model.getUi().component.updataUI(model.getSelected());
             model.getUi().updateToolbarControls(true);
             model.getUi().slidesEditorPane.getChildren().clear();
             //model.getUi().component.reLoad();
             
             
                
             
             
             
             
             
        });
         
        
	
	
    }
    
    
    
    
    
    private void updateCaption(){
        
        page.setCaption(captionTextField.getText());
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    
}
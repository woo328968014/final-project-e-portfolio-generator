package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.SlideShowModel;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class EditorSlidesView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    VBox workSpaceVBox;
    Label captionLabel;
    Label caption;
    
  
    TextField captionTextField;
   
    String userCaption;
    
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public EditorSlidesView(Slide initSlide,SlideShowModel model) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
       // System.out.print(initSlide.getCaptionEntered());
        
        
	
                
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlidesImage();

	// SETUP THE CAPTION CONTROLS
       
	captionVBox = new VBox();
	
	captionLabel = new Label("Enter Caption(Enter to save)");
	captionVBox.getChildren().add(captionLabel);
        if(!slide.getcaption().isEmpty())
            initSlide.setCaptionEntered(true);
        
        if(initSlide.getCaptionEntered()==false){
            captionTextField = new TextField();
            captionVBox.getChildren().add(captionTextField);
            
        }else{
            captionTextField = new TextField();
            //captionTextField.setText(slide.getcaption());
            captionVBox.getChildren().add(captionTextField);
        }
        
       

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        

	// SETUP THE EVENT HANDLERS
       if(!captionTextField.isFocused()){
           initSlide.setCaptionEntered(true);
           this.updateCaption();
           
       }
       
       captionTextField.setOnMouseClicked(e->{
           if(captionTextField.getText().equals("ENTER CAPTION"))
               captionTextField.clear();
       });
       
       captionTextField.setOnAction(e->{
          
           initSlide.setCaptionEntered(true);
           this.updateCaption();
           
           captionTextField.setDisable(true);
       }
       );
        
        
        
       captionVBox.setOnMouseClicked(e->{
               
               model.setSelectedSlide(initSlide);
              // this.setStyle("-fx-background-color: blue");
              model.getUi().reloadSlideShowPane(model);
              
              
        });
         
        
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	imageController.processSelectImage(slide, this);
	});
    }
    
    private void updateCaption(){
        
        slide.setCaption(captionTextField.getText());
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlidesImage() {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image
	}
    }    
}
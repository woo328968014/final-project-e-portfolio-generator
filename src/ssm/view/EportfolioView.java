package ssm.view;
import ssm.model.Page;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import javafx.scene.layout.VBox;

import javafx.stage.Screen;
import javafx.stage.Stage; 

import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;

import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;

import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_EXPORT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;

import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.model.PageModel;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & _____________
 */
public class EportfolioView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;
   
       

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button saveAsSlideShowButton;
    Button exportSlideShowButton;
    Button viewSlideShowButton;
    Button viewEditorButton;
    Button exitButton;
    Button layout1;
    Button layout2;
    Button layout3;
    Button layout4;
    Button layout5;
   Button font1;
   Button font2;
   Button font3;
   Button font4;
   Button font5;
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox pageEditToolbar;
    VBox modeToolbar;
    VBox viewToolbar;
   VBox slideEditToolbar2;
           
    Button addSlideButton;
    Button removeSlideButton;
    Button upSlideButton;
    Button downSlideButton;
    
    Button addPageButton ;
    Button removePageButton;
    Button upPageButton;
    Button downPageButton;
    Button color1;
            Button color2;
            Button color3;
            Button color4;
            Button color5;
    Button savePage;
    // AND THIS WILL GO IN THE CENTER
    ScrollPane pagesEditorScrollPane;
    public VBox pagesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    private ScrollPane EditorWorkSpaceScrollPane;
    private ScrollPane slidesEditorScrollPane ;
   
    public VBox workSpaceVBox;
    public VBox slidesEditorPane;
    Label title;
    Label studentName;
    Label banner;
    
    
    
    
    ImageView bannerView;
    TextField titleField;
    TextField nameField;
    ScrollPane ComponentScrollPane;
    Button headerButton;
    Button pButton;
    Button listButton;
    Button addImgButton;
    Button addVidButton;
    Button addSlidesButton;
    
   public ComponentView component;
    private Button eraseButton;
    private VBox compToolbar;
   PageModel selectedPage;
    PageModel pageModel;
    private Button loadBanner;
    private Label footer;
    private TextField footerField;
    private Label color;
    private Label font;
    private Label layout;
    private ImageSelectionController imageController;
    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public EportfolioView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
        ComponentScrollPane=new ScrollPane();
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);
        pageModel= new PageModel(this);
        selectedPage=null;

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    
    public PageModel getPage(){
        return pageModel;
    }
    
    
    public SlideShowModel getSlideShow() {
	return slideShow;
    }
    
    public void setSelectedPage(PageModel page){
        selectedPage=page;
    }
    
    public PageModel getSelectedPage( ){
        return selectedPage;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	slideEditToolbar2 = new VBox();
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        slideEditToolbar2.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	
	
	addSlideButton = this.initChildButton(slideEditToolbar2,		ICON_ADD_SLIDE,	    TOOLTIP_ADD_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removeSlideButton= this.initChildButton(slideEditToolbar2,	ICON_REMOVE_SLIDE,	    TOOLTIP_REMOVE_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        upSlideButton= this.initChildButton(slideEditToolbar2,		ICON_MOVE_UP,	    TOOLTIP_MOVE_UP,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        downSlideButton= this.initChildButton(slideEditToolbar2,		ICON_MOVE_DOWN,	    TOOLTIP_MOVE_DOWN,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        
        compToolbar = new VBox();
	compToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
        eraseButton= this.initChildButton(compToolbar,	ICON_REMOVE_SLIDE,	    "Remove component",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        
        pageEditToolbar = new VBox();
	pageEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
        addPageButton = this.initChildButton(pageEditToolbar,		ICON_ADD_SLIDE,	    "Add Slides",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removePageButton= this.initChildButton(pageEditToolbar,	ICON_REMOVE_SLIDE,	    "Remove Slide",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        upPageButton= this.initChildButton(pageEditToolbar,		ICON_MOVE_UP,	    "Move up",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        downPageButton = this.initChildButton(pageEditToolbar,		ICON_MOVE_DOWN,	    "Move down",	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        
        modeToolbar = new VBox();
        modeToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
        viewSlideShowButton = initChildButton(modeToolbar, ICON_VIEW_SLIDE_SHOW,	"Viewer Workspace",    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        viewEditorButton = initChildButton(modeToolbar, ICON_NEXT,	"Editor Workspace",    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        
	// AND THIS WILL GO IN THE CENTER
	
        pagesEditorPane = new VBox();
            pagesEditorScrollPane = new ScrollPane(pagesEditorPane);

            slidesEditorPane = new VBox();
            slidesEditorScrollPane = new ScrollPane(slidesEditorPane);


            workSpaceVBox = new VBox();
            workSpaceVBox.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);

            title=new Label("Title:");
            titleField = new TextField();
           
            
            studentName=new Label("\nStudent Name:");
            nameField=new TextField();
            
            
            
            banner=new Label("\nSet Banner");
            loadBanner=new Button("Chose file");
            bannerView=new ImageView();


            footer = new Label("\nEnter footer:");
            footerField = new TextField();
            

            layout = new Label("\nChose Layout:");
            layout1=new Button("layout1");
            layout2=new Button("layout2");
            layout3=new Button("layout3");
            layout4=new Button("layout4");
            layout5=new Button("layout5");
            
            imageController = new ImageSelectionController();
            
            
            HBox layoutHBox=new HBox();
            layoutHBox.getChildren().add(layout1);
            layoutHBox.getChildren().add(layout2);
            layoutHBox.getChildren().add(layout3);
            layoutHBox.getChildren().add(layout4);
            layoutHBox.getChildren().add(layout5);


            font = new Label("\nChose Font:");
            font1=new Button("font1");
            font2=new Button("font2");
            font3=new Button("font3");
            font4=new Button("font4");
            font5=new Button("font5");
            
            

            HBox fontHBox=new HBox();
            fontHBox.getChildren().add(font1);
            fontHBox.getChildren().add(font2);
            fontHBox.getChildren().add(font3);
            fontHBox.getChildren().add(font4);
            fontHBox.getChildren().add(font5);


            color = new Label("\nChose Color:");
            color1=new Button("color1");
            color2=new Button("color2");
            color3=new Button("color3");
            color4=new Button("color4");
            color5=new Button("color5");
            
            
            
            HBox colorHBox=new HBox();
            colorHBox.getChildren().add(color1);
            colorHBox.getChildren().add(color2);
            colorHBox.getChildren().add(color3);
            colorHBox.getChildren().add(color4);
            colorHBox.getChildren().add(color5);


            headerButton=new Button("Header");
            pButton=new Button("Paragraph");;
            listButton=new Button("List");;
            addImgButton=new Button("Image");;
            addVidButton=new Button("Video");;
            addSlidesButton=new Button("Slide");;

            HBox buttonHBox=new HBox();
            buttonHBox.getChildren().add(headerButton);
            buttonHBox.getChildren().add(pButton);

            buttonHBox.getChildren().add(listButton);
            buttonHBox.getChildren().add(addImgButton);
            buttonHBox.getChildren().add(addVidButton);
            buttonHBox.getChildren().add(addSlidesButton);

            Label addLabel=new Label("\nAdd Component: ");
        EditorWorkSpaceScrollPane = new ScrollPane(workSpaceVBox);  

            savePage=new Button("Save all changes");

            workSpaceVBox.getChildren().add(title);
            workSpaceVBox.getChildren().add(titleField);
            workSpaceVBox.getChildren().add(studentName);
            workSpaceVBox.getChildren().add(nameField);
            workSpaceVBox.getChildren().add(banner);
            workSpaceVBox.getChildren().add(loadBanner);
            workSpaceVBox.getChildren().add(footer);
            workSpaceVBox.getChildren().add(footerField);
            workSpaceVBox.getChildren().add(layout);
            workSpaceVBox.getChildren().add(layoutHBox);
            workSpaceVBox.getChildren().add(color);
            workSpaceVBox.getChildren().add(colorHBox);
            workSpaceVBox.getChildren().add(font);
            workSpaceVBox.getChildren().add(fontHBox);
            workSpaceVBox.getChildren().add(addLabel);
            workSpaceVBox.getChildren().add(buttonHBox);
            workSpaceVBox.getChildren().add(savePage);
        //this.updatePageInf();
        
    
        
       this.updataUI();
        
	// NOW PUT THESE TWO IN THE WORKSPACE
	
    }
    
    public void updatePageInf(){
        
        
        
        //if(true){
            
        
        
        
           
       
           
            
            
      
    }
    
    
    public void getData(){
        pageModel.getSelected().title=(titleField.getText());
        pageModel.getSelected().name=(nameField.getText());
        pageModel.getSelected().footer=(footerField.getText());
        pageModel.getSelected().comps.clear();
        pageModel.getSelected().comps.addAll(component.comp);
        System.out.println(pageModel.getSelected().comps.size());
       // pageModel.getSelected().layout;
        //pageModel.getSelected().color;
        //pageModel.getSelected().font;
    }
    
    public void setData(){
        
    } 
   
    
    
    public void updataUI(){
        workspace.getChildren().clear();
        workspace.getChildren().add(pageEditToolbar);
	workspace.getChildren().add(pagesEditorScrollPane);
        workspace.getChildren().add(modeToolbar);
        
        workspace.getChildren().add(EditorWorkSpaceScrollPane);
        workspace.getChildren().add(compToolbar);
        workspace.getChildren().add(ComponentScrollPane);
       // workspace.getChildren().add(slideEditToolbar2);
	workspace.getChildren().add(slidesEditorScrollPane);
    }
    
    public void updateWorkspace(EportfolioView ui) {
        ssmPane.setCenter(workspace);
	int index,size;
        index=ui.getPage().getPages().indexOf( ui.getPage().getSelected());
        size=ui.getPage().getPages().size();
	if(size>0)
            removePageButton.setDisable(false);
        else
            removePageButton.setDisable(true);
        
        if(index==size-1)
            downPageButton.setDisable(true);
        else
            downPageButton.setDisable(false);
        
        if(index==0 || size==0)
            upPageButton.setDisable(true);
        else
            upPageButton.setDisable(false);
        
        
    }
    
    

    

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> { 
	    fileController.handleNewSlideShowRequest();
           
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
        viewSlideShowButton.setOnAction(e -> {
	    fileController.handleViewSlideShowRequest();
	});
        
        exportSlideShowButton.setOnAction(e -> {
	    fileController.handleExport();
	});
        
        
        
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	
        addSlideButton.setOnAction(e -> {
	    //editController.processAddSlideShowRequest();
            
           
	});
        
        addPageButton.setOnAction(e -> {
	    editController.processAddPageRequest();
            System.out.println("ADDed Page");
           
	});
        
        removePageButton.setOnAction(e -> {
	    editController.processRemovePageRequest();
	});
        upPageButton.setOnAction(e -> {
	    editController.processMoveUpSlideRequest();
	});
        downPageButton.setOnAction(e -> {
	    editController.processMoveDownSlideRequest();
	});
        
        savePage.setOnAction(e ->{
          this.getData();
          
        });
        
        loadBanner.setOnAction(e ->{
            imageController.processSelectImage(pageModel.getSelected());
            
        });
        
         component=new ComponentView(this.pageModel,this);
        headerButton.setOnAction(e ->{
           
          component.addHeader();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          this.updateToolbarControls(false);  
        });
        pButton.setOnAction(e ->{
          component.addParagraph();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          this.updateToolbarControls(false);  
        });
        
        listButton.setOnAction(e ->{
          component.addList();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          this.updateToolbarControls(false);  
        });
        
        addImgButton.setOnAction(e ->{
          component.addImage();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          this.updateToolbarControls(false);  
        });
        
        addVidButton.setOnAction(e ->{
          component.addVideo();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          this.updateToolbarControls(false);  
        });
        addSlidesButton.setOnAction(e ->{
          component.addSlides();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          this.updateToolbarControls(false);  
        });
        
         eraseButton.setOnAction(e ->{
          component.removeSelected();
          ComponentScrollPane=new ScrollPane(component.updataUI());
          this.updataUI();
          
            
        });
         
           layout1.setOnMouseClicked(e->{
                pageModel.getSelected().layout="1";
                reloadPageInf(pageModel);
            });
            layout2.setOnMouseClicked(e->{
                pageModel.getSelected().layout="2";reloadPageInf(pageModel);
            });
            layout3.setOnMouseClicked(e->{
                pageModel.getSelected().layout="3";reloadPageInf(pageModel);
            });
            layout4.setOnMouseClicked(e->{
                pageModel.getSelected().layout="4";reloadPageInf(pageModel);
            });
            layout5.setOnMouseClicked(e->{
                pageModel.getSelected().layout="5";reloadPageInf(pageModel);
            });
        
            font1.setOnMouseClicked(e->{
                pageModel.getSelected().font="verdana";reloadPageInf(pageModel);
            });
            font2.setOnMouseClicked(e->{
                pageModel.getSelected().font="courier";reloadPageInf(pageModel);
            });
            font3.setOnMouseClicked(e->{
                pageModel.getSelected().font="Geneva";reloadPageInf(pageModel);
            });
            font4.setOnMouseClicked(e->{
                pageModel.getSelected().font="Times New Roman";reloadPageInf(pageModel);
            });
            font5.setOnMouseClicked(e->{
                pageModel.getSelected().font="Palatino Linotype";reloadPageInf(pageModel);
            });

            color1.setOnMouseClicked(e->{
                pageModel.getSelected().color="white";reloadPageInf(pageModel);
            });
            color2.setOnMouseClicked(e->{
                pageModel.getSelected().color="blue";reloadPageInf(pageModel);
            });
            color3.setOnMouseClicked(e->{
                pageModel.getSelected().color="yellow";reloadPageInf(pageModel);
            });
            color4.setOnMouseClicked(e->{
                pageModel.getSelected().color="green";reloadPageInf(pageModel);
            });
            color5.setOnMouseClicked(e->{
                pageModel.getSelected().color="red";reloadPageInf(pageModel);
            });
        // THEN THE SLIDE SHOW VIEW CONTROLS
	
        
        
        
        
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        saveAsSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	"Save as",    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportSlideShowButton = initChildButton(fileToolbarPane, ICON_EXPORT,	"Export",    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);
        //steal ICON and set
        
        String imagePath = "file:" + PATH_ICONS + ICON_VIEW_SLIDE_SHOW;
        primaryStage.getIcons().add(new Image(imagePath));
	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,  
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
        saveAsSlideShowButton.setDisable(saved);
        exportSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
        addPageButton.setDisable(false);
        if(this.getPage().getPages().size()>0){
            viewSlideShowButton.setDisable(false);
        }else
             viewSlideShowButton.setDisable(true);
        System.out.println("Size  "+this.component.pane.size());
        if(this.component.pane.size()>0){
            eraseButton.setDisable(false);
        }else
            eraseButton.setDisable(true);
        
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadComp(){
        component.getChildren().clear();
        component.updataUI();
    }
    
    
   
    
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
             EditorSlidesView slideEditor = new EditorSlidesView(slide,slideShowToLoad);
            if(slideShowToLoad.getSelectedSlide().equals(slide))
                slideEditor.setStyle("-fx-background-color: rgb(50, 150, 930)");
	    slidesEditorPane.getChildren().add(slideEditor);
            this.updateWorkspace(this);
            //  *************hightlight
	}
    }
    
   public void reloadPagePane(PageModel pageToLoad) {
	pagesEditorPane.getChildren().clear();
	for (Page page : pageToLoad.getPages()) {
            PageEditView slideEditor = new PageEditView(page,pageToLoad);
            
            if(pageToLoad.getSelected().equals(page))
                slideEditor.setStyle("-fx-background-color: rgb(50, 150, 930)");
	    pagesEditorPane.getChildren().add(slideEditor);
            this.updateWorkspace(this);
            //  *************hightlight
	}
    }
   
   public void reloadPageInf(PageModel pageToLoad) {
       //workSpaceVBox.getChildren().clear();
       
        if(pageModel.getSelected()!=null){
            
           

            footerField.setText(pageModel.getSelected().footer);
            nameField.setText(pageModel.getSelected().name); 
            titleField.setText(pageModel.getSelected().title);
            layout.setText("Chose Layout: "+pageModel.getSelected().layout);
            color.setText("Chose color: "+pageModel.getSelected().color);
            font.setText("Chose font: "+pageModel.getSelected().font);   
                
            }
        else
            this.initWorkspace();
       
       
   }
   
   
   
   
}
